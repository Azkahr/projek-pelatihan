@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="/backend/app-assets/css/uploader.css">
    <link rel="stylesheet" href="/backend/app-assets/css/uploader-2.css">

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Your Page</a>
                            </li>
                            <li class="breadcrumb-item active">
                                @if ($data == null)
                                Create Your Page
                                @else
                                Update Your Page
                                @endif
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body">
    <!-- Validation -->
        <section class="bs-validation">
            @if ($data !== null)
                <form action="{{ route('admin.update', $data->id) }}" id="jquery-val-form" method="POST" enctype="multipart/form-data"> <!-- jika form edit -->
                    @method('PUT')
            @else
                <form action="{{ route('admin.store') }}" method="POST" enctype="multipart/form-data"> <!-- jika form tambah -->
            @endif
                @csrf
                <div class="row">
                    <!-- Bootstrap Validation -->
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    @if ($data == null)
                                        Tambah data kelas
                                    @else
                                        Update data kelas
                                    @endif
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" id="name" class="form-control @error('name') border-danger @enderror" value="{{ $data ? $data->name : '' }}">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="jurusan" class="form-label">Jurusan</label>
                                    <select class="form-select form-control" name="jurusan_id" id="jurusan_id" value="{{ old('jurusan_id') }}">
                                        @foreach ($jurusan as $jur)
                                            @if ($data && $data->jurusan_id == $jur->id)
                                                <option value="{{ $jur->id }}" selected>{{ $jur->nama_jurusan }}</option>
                                            @else
                                                @if (old('jurusan_id') == $jur->id)
                                                    <option value="{{ $jur->id }}" selected>{{ $jur->nama_jurusan }}</option>
                                                @else
                                                    <option value="{{ $jur->id }}">{{ $jur->nama_jurusan }}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-md-6">
                        <button type="submit"
                        data-initial-text="Update <i class='icon-paperplane ml-2'></i>"
                        data-loading-text="<i class='fas fa-spinner fa-spin'></i> Loading..."
                        class="btn btn-primary"> Save Changes </button>
                    </div>
                </div>
            </form>
        </section>
    </div>

@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        $(function(){
            var textarea1 = document.getElementById('description');
            CKEDITOR.replace(textarea1);
        })
    </script>
    <script>

        function readURL2(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                $('.image-upload-wrap-2').hide();

                $('.file-upload-image-2').attr('src', e.target.result);
                $('.file-upload-content-2').show();

                $('.image-title-2').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload2();
            }
        }

        function removeUpload2() {
            $('.file-upload-input-2').replaceWith($('.file-upload-input-2').clone());
            $('.file-upload-content-2').hide();
            $('.image-upload-wrap-2').show();
        }

        $(function (){
            $('.image-upload-wrap-2').bind('dragover', function () {
                $('.image-upload-wrap-2').addClass('image-dropping-2');
            });

            $('.image-upload-wrap-2').bind('dragleave', function () {
                    $('.image-upload-wrap-2').removeClass('image-dropping-2');
            });
        });
    </script>
    @if(Session::get('create'))
        <script type="text/javascript">
            $(document).ready(function(){

             // Success Type
                toastr['success']('Successfully Create Data.', 'Successfully', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });

            });
        </script>
    @endif

    @if(Session::get('update'))
    <script type="text/javascript">
        $(document).ready(function(){

            // Success Type
            toastr['success']('Successfully Update Data.', 'Successfully', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });

        });
    </script>
    @endif

@endsection