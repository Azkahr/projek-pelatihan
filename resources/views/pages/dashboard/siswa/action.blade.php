@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="/backend/app-assets/css/uploader.css">
    <link rel="stylesheet" href="/backend/app-assets/css/uploader-2.css">

    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.admin') }}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('admin.siswa.index') }}">Siswa</a></li>
                            </li>
                            <li class="breadcrumb-item active">
                                @if ($data == null)
                                Add Data
                                @else
                                Update Data
                                @endif
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card p-2">
    <!-- Validation -->
            @if ($data !== null)
            <form action="{{ route('admin.siswa.update', $data->id) }}" method="POST"> <!-- jika form edit -->
                @method('PUT')
            @else
            <form action="{{ route('admin.siswa.store') }}" method="POST">  <!-- jika form tambah -->
            @endif
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" value="{{ $data ?  $data->name : "" }}" id="name" placeholder="Enter Name" name="name">
                </div>

                <div class="form-group">
                    <label for="nis">NIS</label>
                    <input type="text" class="form-control" value="{{ $data ? $data->nis : "" }}" id="nis" placeholder="Enter Nomor Induk Siswa" name="nis">
                </div>

                <div class="form-group">
                    <label for="dob">Date of Birth</label>
                    <input type="date" class="form-control" id="dob" value="{{ $data ? $data->dob : "" }}" placeholder="Enter Date of Birth" name="dob">
                </div>

                <div class="form-group">
                    <label for="kelas">Gender</label>
                    <select class="custom-select" name="gender" id="gender">\
                        <option {{ $data ? $data->gender == "1" ? "selected" : "" : "" }} value="1">Laki-Laki</option>
                        <option {{ $data ? $data->gender == "2" ? "selected" : "" : "" }} value="2">Perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="kelas">Kelas</label>
                    <select class="custom-select" name="kelas_id" id="kelas">
                        @foreach ($kelas as $item)
                            @if ($data && $data->kelas->id === $item->id )
                                <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                            @else
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        {{-- <section class="bs-validation">
            @if (!$data !== null)
            <form action="" method="POST" enctype="multipart/form-data"> <!-- jika form tambah -->
            @else
            <form action="" id="jquery-val-form" method="POST" enctype="multipart/form-data"> <!-- jika form edit -->
            @endif
                @csrf
                <div class="row">
                    <!-- Bootstrap Validation -->
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">
                                    @if ($data == null)
                                    Create Your Page
                                    @else
                                    Update Your Page
                                    @endif
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="file-upload-2  @error('image') border-danger @enderror">
                                    <button class="file-upload-btn-2" type="button" onclick="$('.file-upload-input-2').trigger( 'click' )">Add Image</button>
                                    <div class="image-upload-wrap-2">
                                        <input class="file-upload-input-2" type='file' onchange="readURL2(this);" accept="image/*" name="image" />
                                        <div class="drag-text-2">
                                            @if ($data !== null)
                                                <img class="file-upload-image-2" src="{{ $data ? $data->image : '' }}" alt="your image" />
                                            @else
                                                <h3>Drag and drop a file or select add image</h3>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="file-upload-content-2">
                                        <img class="file-upload-image-2" src="" alt="your image" />
                                        <div class="image-title-wrap-2">
                                            <button type="button" onclick="removeUpload2()" class="remove-image-2">Remove <span class="image-title">Uploaded Icons</span></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Input Text</label>
                                    <input type="text" name="name" class="form-control @error('name') border-danger @enderror" value="{{ $data ? $data->name : '' }}">
                                    @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">CK EDITOR</label>
                                    <textarea name="description" class="form-control" id="description" rows="10">{{ $data ? $data->description : '' }}</textarea>
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-md-6">
                        <button type="submit"
                        data-initial-text="Update <i class='icon-paperplane ml-2'></i>"
                        data-loading-text="<i class='fas fa-spinner fa-spin'></i> Loading..."
                        class="btn btn-primary"> Save Changes </button>
                    </div>
                </div>
            </form>
        </section> --}}
    </div>

@endsection

{{-- @section('script')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        $(function(){
            var textarea1 = document.getElementById('description');
            CKEDITOR.replace(textarea1);
        })
    </script>
    <script>

        function readURL2(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {
                $('.image-upload-wrap-2').hide();

                $('.file-upload-image-2').attr('src', e.target.result);
                $('.file-upload-content-2').show();

                $('.image-title-2').html(input.files[0].name);
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload2();
            }
        }

        function removeUpload2() {
            $('.file-upload-input-2').replaceWith($('.file-upload-input-2').clone());
            $('.file-upload-content-2').hide();
            $('.image-upload-wrap-2').show();
        }

        $(function (){
            $('.image-upload-wrap-2').bind('dragover', function () {
                $('.image-upload-wrap-2').addClass('image-dropping-2');
            });

            $('.image-upload-wrap-2').bind('dragleave', function () {
                    $('.image-upload-wrap-2').removeClass('image-dropping-2');
            });
        });
    </script>
    @if(Session::get('create'))
        <script type="text/javascript">
            $(document).ready(function(){

             // Success Type
                toastr['success']('Successfully Create Data.', 'Successfully', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });

            });
        </script>
    @endif

    @if(Session::get('update'))
    <script type="text/javascript">
        $(document).ready(function(){

            // Success Type
            toastr['success']('Successfully Update Data.', 'Successfully', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });

        });
    </script>
    @endif

@endsection --}}