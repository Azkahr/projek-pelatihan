<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ url('/') }}">
                <span class="brand-logo">
                    <img src="/logo.svg" style="max-width: 147px !important; margin-left: 22px;margin-bottom: 20px !important;">
                </span>
                {{-- <h2 class="brand-text">Shirvano</h2> --}}
            </a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header"><span data-i18n="Apps &amp; Pages">  Name Application</span><i data-feather="more-horizontal"></i>
            </li>

            <li class="nav-item {{ Route::is("admin.admin") ? "active" : "" }}">
                <a class="d-flex align-items-center" href="{{ route('admin.admin') }}">
                    <i data-feather='cast'></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Home</span>
                </a>
            </li>

            <li class="nav-item {{ Route::is(['admin.siswa.index', 'admin.siswa.create', 'admin.siswa.edit']) ? "active" : "" }}">
                <a class="d-flex align-items-center" href="{{ route('admin.siswa.index') }}">
                    <i data-feather='cast'></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Siswa</span>
                </a>
            </li>
            <li>
                <a class="d-flex align-items-center" href="{{ route('admin.jurusan.jurusan') }}">
                    <i data-feather='cast'></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Jurusan</span>
                </a>
                </li>
            <li class="nav-item {{ Route::is(['admin.kelas', 'admin.edit']) ? "active" : "" }}">
                <a class="d-flex align-items-center" href="{{ route('admin.kelas') }}">
                    <i data-feather='cast'></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Kelas</span>
                </a>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='home'></i><span class="menu-title text-truncate" data-i18n="Invoice">Menu</span></a>
                <ul class="menu-content">
                    <li class="">
                        <a class="d-flex align-items-center" href="">
                            <i data-feather='image'></i>
                            <span class="menu-title text-truncate" data-i18n="Dashboard">Sub Menu</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu -->