@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
        @if ($data)
            <form action="{{ route('admin.jurusan.ubahJurusan', $data->id) }}" method="post">
        @endif
        <form action="{{ route('admin.jurusan.simpanJurusan') }}" method="post">
            @csrf
            <input type="text" name="nama_jurusan" class="form-control" value="{{ $data ? $data->nama_jurusan : " " }}">
            <button type="submit" class="btn btn-primary mt-2     ">Simpan</button>
        </form>  
    </div>
</div>
@endsection