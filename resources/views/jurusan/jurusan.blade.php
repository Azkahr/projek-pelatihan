@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-body">
            <a href="{{ route('admin.jurusan.tambahJurusan') }}" class="btn btn-primary">Tambah</a>
            <table class="table" style="margin-top: 20px;">
                <thead>
                    <tr>
                      <th scope="col">Id</th>
                      <th scope="col">Nama Jurusan</th>
                      <th scope="col">Section</th>
                    </tr>
                  </thead>
                  <tbody>
                      @foreach ($jurusan as $key => $item)
                    <form action="{{ route('admin.jurusan.hapusJurusan', $item->id) }}" method="post">
                        @csrf
                      <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $item->nama_jurusan }}</td>
                        <td>
                            <a href="{{ route('admin.jurusan.editJurusan', $item->id) }}" class="btn btn-warning">Edit</a>
                            <button class="btn btn-danger" type="submit">Hapus</button>
                        </td>
                      </tr>
                    </form>
                      @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection