<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Jurusan;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      => 'Development',
            'email'     => 'admin@development.com',
            'password'  => bcrypt('admin123')
        ]);

        User::create([
            'name'      => 'Name Project',
            'email'     => 'admin@education.com',
            'password'  => bcrypt('admin123')
        ]);

        Jurusan::create([
            'nama_jurusan' => 'Rekayasa Perangkat Lunak',
        ]);

        Jurusan::create([
            'nama_jurusan' => 'Teknik Komputer dan Jaringan',
        ]);
    }
}
