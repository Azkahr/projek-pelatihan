<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\FAQController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\JoinController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\BecomeController;
use App\Http\Controllers\Api\SocialController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\ProgramController;
use App\Http\Controllers\Api\TestimoniController;
use App\Http\Controllers\Api\PreferenceController;
use Symfony\Component\HttpKernel\Profiler\Profile;
use App\Http\Controllers\Api\ApplicationController;
use App\Http\Controllers\Api\UniversitiesController;
use App\Http\Controllers\Synch\LoginController as SynchLoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/',function () {
    return response()->json([
        'Application'   => 'Name Project',
        'Version'       => 'V.01.00.000'
    ]);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
