<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\ExampleController;
use App\Http\Controllers\KelasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [ExampleController::class , 'index']);
Route::group(['middleware' => 'auth', 'prefix' => 'admin' , 'as' => 'admin.'], function(){
    Route::get('/', [ExampleController::class, 'admin'])->name('admin');

    Route::get('/kelas', [KelasController::class, 'index'])->name('kelas');
    Route::get('/create', [KelasController::class, 'create'])->name('create');
    Route::post('/create', [KelasController::class, 'store'])->name('store');

    Route::get('/update/{kelas:id}', [KelasController::class, 'edit'])->name('edit');
    Route::put('/update/{id}', [KelasController::class, 'update'])->name('update');
    
    Route::delete('/delete/{id}', [KelasController::class, 'destroy'])->name('destroy');
    
    Route::group(['prefix' => 'siswa', 'as' => 'siswa.'], function(){
        Route::get('/', [SiswaController::class, 'index'])->name('index');
        Route::get('/create', [SiswaController::class, 'create'])->name('create');
        Route::post('/', [SiswaController::class, 'store'])->name('store');
        Route::get('/{siswa}', [SiswaController::class, 'edit'])->name('edit');
        Route::put('/{siswa}', [SiswaController::class, 'update'])->name('update');
        Route::delete('/{siswa}', [SiswaController::class, 'destroy'])->name('delete');
    });
    Route::group(['prefix' => 'jurusan', 'as' => 'jurusan.'], function(){
        Route::get('/', [JurusanController::class, 'jurusan'])->name('jurusan');
        Route::post('/simpanjurusan', [JurusanController::class, 'simpan'])->name('simpanJurusan');
        Route::post('/hapusjurusan/{id}', [JurusanController::class, 'hapus'])->name('hapusJurusan');
        Route::get('/editjurusan/{jurusan:id}', [JurusanController::class, 'edit'])->name('editJurusan');
        Route::get('/tambahjurusan', [JurusanController::class, 'tambah'])->name('tambahJurusan');
        Route::post('/ubahjurusan/{id}', [JurusanController::class, 'ubah'])->name('ubahJurusan');
    });

    Route::get('/kelas/create', [KelasController::class, 'create'])->name('create');
});

Route::get('/debug', [SiswaController::class, 'index']);
