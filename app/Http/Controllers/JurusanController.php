<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use Illuminate\Http\Request;

class JurusanController extends Controller
{
    public function jurusan() {
        $jurusan = Jurusan::all();
        return view('jurusan.jurusan', compact('jurusan'));
    }
    public function simpan(Request $request) {
       Jurusan::create([
        "nama_jurusan" => $request->nama_jurusan
       ]);

       return redirect()->route('admin.jurusan.jurusan');
    }
    public function edit(Jurusan $jurusan) {
        $data = $jurusan;
        return view('jurusan.action', compact('data'));
    }
    public function hapus($id){

        $jurusan = Jurusan::find($id);
        $jurusan->delete();
        return redirect()->route('admin.jurusan.jurusan');
    }
    public function ubah(Request $request , $id){

        $jurusan = Jurusan::find($id);
        $jurusan->update(["nama_jurusan" => $request->nama_jurusan]);
        return redirect()->route('admin.jurusan.jurusan');
    }
    public function tambah(){
        $data = null;
        return view('jurusan.action',compact('data'));
    }

}
