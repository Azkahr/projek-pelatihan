<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Kelas;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index() {
        return view('pages.kelas.index', [
            "kelas" => Kelas::all()
        ]);
    }
    
    public function create(){
        $data = null;

        return view('pages.kelas.action', [
            "data" => $data,
            "jurusan" => Jurusan::all()
        ]);
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'jurusan_id' => 'required'
        ]);

        Kelas::create($validatedData);

        return redirect()->route('admin.admin')->with('success', 'Data kelas berhasil ditambahkan');
    }

    public function edit(Kelas $kelas) {
        $data = $kelas;
        
        return view('pages.kelas.action', [
            "data" => $data,
            "jurusan" => Jurusan::all()
        ]);
    }

    public function update(Request $request) {
        $validatedData = $request->validate([
            'name' => 'required|min:3',
            'jurusan_id' => 'required'
        ]);

        Kelas::where('id', $request->id)->update([
            'name' => $validatedData['name'],
            'jurusan_id' => $validatedData['jurusan_id']
        ]);
        
        return redirect()->route('admin.admin')->with('success', 'Data kelas berhasil diubah');
    }

    public function destroy(Request $request) {
        Kelas::destroy($request->id);
        
        return redirect()->route('admin.admin')->with('success', 'Data kelas berhasil dihapus');
    }
}
